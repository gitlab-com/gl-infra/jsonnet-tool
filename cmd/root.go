package cmd

import (
	"fmt"
	"log"
	"os"
	"runtime/pprof"

	"github.com/spf13/cobra"
)

var cpuProfileFile string
var cpuProfileCleanup func()

func init() {
	rootCmd.PersistentFlags().StringVar(
		&cpuProfileFile, "cpu-profile", "",
		"Perform a CPU profile for the run and write it to the file",
	)
}

var rootCmd = &cobra.Command{
	Use:   "jsonnet-tool",
	Short: "A tool for rendering jsonnet",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		if cpuProfileFile != "" {
			var err error
			cpuProfileCleanup, err = cpuProfile()
			if err != nil {
				return fmt.Errorf("failed to perform cpu profile: %w: %w", errCommandFailed, err)
			}
		}

		return nil
	},
	PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
		if cpuProfileCleanup != nil {
			cpuProfileCleanup()
		}

		return nil
	},
}

// Performs a CPU profile, returning a cleanup function to be called on program exit.
func cpuProfile() (func(), error) {
	log.Printf("cpu profile enabled: writing profile to %s", cpuProfileFile)

	f, err := os.Create(cpuProfileFile)
	if err != nil {
		return nil, fmt.Errorf("could not create CPU profile: %w", err)
	}

	if err := pprof.StartCPUProfile(f); err != nil {
		err2 := f.Close()
		if err2 != nil {
			log.Fatal("could not close CPU profile: ", err2)
		}

		return nil, fmt.Errorf("could not start CPU profile: %w", err)
	}

	return func() {
		pprof.StopCPUProfile()

		err2 := f.Close()
		if err2 != nil {
			log.Fatal("could not close CPU profile: ", err2)
		}
	}, nil
}

// Execute executes the root command.
func Execute() error {
	err := rootCmd.Execute()
	if err != nil {
		return fmt.Errorf("execution failed %w: %w", err, errCommandFailed)
	}

	return nil
}
