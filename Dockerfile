FROM registry.access.redhat.com/ubi9/ubi:9.5-1739751568
ARG BUILD_ID

# scratch binaries stay in the root
COPY ${BUILD_ID} /usr/local/bin/jsonnet-tool

ENTRYPOINT ["/usr/local/bin/jsonnet-tool"]
